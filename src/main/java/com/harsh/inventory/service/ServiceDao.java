package com.harsh.inventory.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.harsh.inventory.model.Entry;
import com.harsh.inventory.repository.InventoryRepository;


@Service
@Transactional
public class ServiceDao {
	
	@Autowired
	InventoryRepository repository;
	
	//private static List<Entry> entries = new ArrayList<>();
	
	
	
	
	public Entry saveInventory(Entry entry) {
		if(entry.getId() != 0) {
			Optional<Entry> inventory = repository.findById(entry.getId());
			if(inventory.isPresent()) {
				Entry newEntry = inventory.get();
				newEntry.setPropertyName(entry.getPropertyName());
				newEntry.setCost(entry.getCost());
				newEntry.setQuantity(entry.getQuantity());
				newEntry.setDescription(entry.getDescription());
				
				newEntry = repository.save(newEntry);
				return newEntry;
			}
		}
		else {
			entry = repository.save(entry);
		}
		return entry;

	}
	

	
	//	Get All inventrory Details
	public List<Entry> findAll(){
		return repository.findAll();
	}
	
	//Get FindOne Name
	public List<Entry> findOne(String propertyName) {
		
		return repository.findByPropertyName(propertyName);
	}
	


	//	delete
	public Entry deleteById(int id) {
		List<Entry> entries =  repository.findAll();
		
		Iterator<Entry> iterator = entries.iterator();
		
		while(iterator.hasNext()) {
			Entry entry = iterator.next();
			if(entry.getId() == id) {
				iterator.remove();
				return entry;
			}
		}
		return null;
	}
	
	//Delete Inventory
	public List<Entry> delete(int id) {
		
		return repository.removeById(id);
	}
	
	
	
}
