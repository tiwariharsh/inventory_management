package com.harsh.inventory.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.harsh.inventory.model.Entry;

import com.harsh.inventory.service.ServiceDao;

import com.harsh.inventory.controller.UserNotFoundException;




@RestController
public class MainController {
	
	@Autowired
	ServiceDao service;

	@PostMapping("/inventory")
	public ResponseEntity<Object> createUser(@Valid @RequestBody Entry entry) {
		Entry savedUser = service.saveInventory(entry);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(savedUser.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
		@GetMapping("/inventory")
		public List<Entry> retrieveAllInventory(){
			return service.findAll();
		}
	
		//	Get Inventory By Name
		@GetMapping("/inventory/{propertyName}")
		public List<Entry> retrieveUser(@PathVariable String propertyName) {
			List<Entry> entry =  service.findOne(propertyName);
			
			if(entry == null) {
				throw new UserNotFoundException("id-"+ propertyName);
			}
			return entry;
		}
		

		
		@DeleteMapping(value = "/inventory/{id}")
	    public String delete(@PathVariable(value = "id") int id){
			List<Entry> entry = service.delete(id);
	        if(entry == null) {
				throw new UserNotFoundException("id-"+ id);
		}
	      return "success";
	    }
	
}
