package com.harsh.inventory.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.harsh.inventory.model.Entry;




public interface InventoryRepository extends JpaRepository<Entry, Long>  {

	Entry save(Entry entry);
	
	List<Entry> findAll();
	
	List<Entry> findByPropertyName(String propertyName);
	
	List<Entry> removeById(int id); 
	
	Optional<Entry> findById(int id);
	
}
