package com.harsh.inventory.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="entry")
public class Entry {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private int id;
	private String propertyName;
	private String cost;
	private String quantity;
	private String description;
	
	
	
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	@Override
	public String toString() {
		return "Entry [id=" + id + ", propertyName=" + propertyName + ", cost=" + cost + ", quantity=" + quantity
				+ ", description=" + description + "]";
	}
	

	
	
}
